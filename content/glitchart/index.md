---
title: "Glitch Art"
draft: false
description: 
---

Ni de broma me considero un artista, solo sé me ocurre una idea, tomo imagenes que puedan encajar, las modifico usando scripts de Processing 2 y junto todo en GIMP hasta tener un resultado que me convenza. Disfruto el proceso y su resultado.
 
{{< gallery match="glitchart/*" sortOrder="desc" rowHeight="150" margins="5" randomize=true thumbnailResizeOptions="600x600 q85 Lanczos" showExif=true previewType="blur" embedPreview=true loadJQuery=true >}}