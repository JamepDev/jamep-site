---
title: "Sobre mí"
type: "about"
---
> [Even though you lost your way\
You never lost your heart](https://www.youtube.com/watch?v=Q1kBLHiD_Tw)

![ProfilePic](profile.jpg#floatleft)

Jamep es un pequeño proyecto que ya tiene algunos años en existencia, primero con una página web en [Neocities](https://neocities.org/site/jamep-site) y actualmente como un canal de [YouTube](https://www.youtube.com/@Jamep_). El núcleo del proyecto ha sido el de compartir información sobre temas que me interesen. Manejado por Javier, un estudiante de Ing. Electrónica, entusiasta de videojuegos y se cree autodidacta en sus tiempos libres.

Esta página es para conocer más a fondo sobre mi persona que a su vez conoces más sobre mi proyecto, porque realmente Jamep es un reflejo de mi persona.


### Contacto
- Correo: <a href = "mailto: jamepWelles@disroot.org">jamepWelles@disroot.org</a></li>


