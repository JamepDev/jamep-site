---
title: "Música"
description: .
---
La música es parte importante de mi persona y proyecto. El cómo puede moldear un sentimiento con un conjunto de frecuencias auditivas es algo que no deja de sorprenderme. Esta sección antes era agregar álbumes que me gustan, pero qué pereza hacerlo cuando ya existen páginas dedicadas a eso, en el caso que solo quieres si saber escuché «x» álbum, pueden ir a mi [RYM](https://rateyourmusic.com/~jamep). Ahora esta sección es más como un pequeño «altar» para este pasatiempo.

---
## Mi colección de CDs

![CDs](coleccion.jpg)
---

