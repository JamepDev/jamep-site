---
title: "Blog"
date: 2023-11-05T14:05:34-06:00
draft: false
---
La sección personal de la página web, aquí subiré mis gustos, 
pensamientos o cualquier cosa que me parezca interesante compartir.
