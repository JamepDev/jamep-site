---
Title: "Inseguridad"
date: 2024-12-06
---
El cometer errores es bastante común. 

> Si algo puede salir mal saldrá mal - Edward A. Murphy.

Justo esta frase es la que pienso muy a menudo cada vez que quiero hacer algo. Casi en cualquier cosa que quiero crear la suelo sobre pensar, puede que «x» o «y» salga mal. Me pasó con la idea de crear este blog y con casi cualquier video que he hecho para el canal de YouTube. Me surge una necesidad de buscar la aprobación de otros para saber que voy en el camino «correcto». ¿Suena horrible?, lo es, es una carga que te paraliza, tratas de buscar una falla, pero realmente no hay ninguna evidencia sobre este supuesto error, y si lo hubiera, probablemente no lo notarías por tener una visión de túnel en tu trabajo, por lo que solo te quedas estancado.

Este sentimiento es de los principales motivos de mis repentinas etapas de inactividad, en redes sociales o en mi canal, siempre que se me ocurre alguna idea en la que quiero trabajar, trato de planearlo, estructurar mis puntos y creo mi primer borrador. Desde ese punto en adelante solo empeora mi confianza en mi idea, las primeras dudas e inseguridades empiezan a aparecer, inseguridades tan nimias como elegir una palabra correcta hasta cuestionar toda la idea que tenía planteada. Sin duda alguna esto último es la parte más tediosa de terminar un video. Pero al menos queda mejor, ¿no?, para nada, solo terminas más quemado por el trabajo, lo que empeora como trabajas. Con el ritmo que he tenido de hacer videos, este sentimiento, aunque presente, ya no es tan fuerte. Me centro más en como terminar el proyecto, si tengo una crítica argumentada, bienvenida sea, pero hasta ahí. No es el fin del mundo si me equivoco (No válido para proyectos de ingeniería). 

En mis cortos 22 años de existencia he aprendido que algo peor a un error es no haberlo intentado, siempre viene a mi mente una canción de Jim O’Rourke. El primer coro para ser exactos: 

> [If I were die with this things on, can't say I didn't try](https://www.youtube.com/watch?v=wnw7-YfSaQg)

El arrepentimiento de no haberlo intentado es mayor al de equivocarse, con todo los errores que puedas cometer, al menos puedes sacar algo de ello y aprender de este, algo que no tendrás si no lo intentas.
