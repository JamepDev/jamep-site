---
title: "Videojuegos"
---
Todo el registro de lo que juego lo llevo en [backloggd](https://backloggd.com/u/Jamep/), 
por si lo que quieres ver son mis gustos. En esta sección pondré blogs sobre temas que no hablaría en mi canal de YouTube, porque son pensamientos cortos o porque no les encuentro que encaje en mi formato.
