---
title: "¿Por qué Ocarina of Time es tan aclamado?"
date: 2023-11-08T20:12:32-06:00
draft: false
---
Siempre me he preguntado, porque The Legend of Zelda: Ocarina of Time es uno de los mejores videojuegos de la historia,
no es que odie el juego, pero, en serio, ¿A partir de la segunda década de los 90 salieron los mejores videojuegos jamás creados?
¿Que pasa con todos los videojuegos anteriores a esta época del 3D? Pues bueno, investigando para un futuro video encontré un patrón curioso, 
la mayoría de premiaciones, ya sea por organizaciones o por revistas, hicieron sus primeros eventos en 1997-1998.

![bafta](bafta.png)
![cesa](cesa.png)
![dice](DICE.png)
![edge](edge.png)
![gamespot](gamspot.png)
![spotlight](spotlight.png)
![gameinformer](gameinformer.png)
![electronicgamingmonthly](egm.png)
![vsda](vsda.png)

_Solo tres de nueve premiaciones no se crearon entre 1997-1998._

Es cuando vi que por eso se habla muy poco de arcades o de los videojuegos de generaciones pasadas,
tienen menor “prestigio” por no tener ninguna premiación en su época. 

Tal vez no sea la única razón, pero tiene bastante sentido para mí.
