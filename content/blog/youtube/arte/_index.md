---
title: "Los Videojuegos NO Necesitan Ser ARTE"
date: 2023-01-14
draft: false
---
{{<youtube QmBDVIqYKPk>}}

Este video nace de querer aportar algo nuevo a esta conversación tan quemada, 
por mi deseo de dar una nueva perspectiva a este asunto y tirar un poco estas creencias que se han tenido por bastante tiempo. 
Este ha sido uno de los videos más sinceros que he hecho.

{{< ko-fi >}}


Música:
* [The 25th Ward: The Silver Case OST - Finding My Way](https://www.youtube.com/watch?v=FwQyyU7fUuQ)
* [DJ Shadow - Strike 1](https://youtu.be/Jv4Lh6c4DNk)
* [DJ Shadow - What Does Your Soul Look Like, Pt. 3](https://www.youtube.com/watch?v=m9VIDrDXJmc)
* [J Dilla - The Diff'rence](https://www.youtube.com/watch?v=_Hhjh1pYOOo)
* [GoGo Peguin - Garden Dog Barbecue](https://www.youtube.com/watch?v=LAz4dZfABPk)
* [Bruno Pernadas - Spaceway 70](https://www.youtube.com/watch?v=GRdYqxakKIE)
* [Kokuga OST - 萌芽 (Theme 2)](https://youtu.be/V-qWH1Qrsq0)
* [MUTEMATH - Azteca](https://www.youtube.com/watch?v=Se7Lb-fi_dE)
* [MUTEMATH - Work Of Art](https://www.youtube.com/watch?v=Q1kBLHiD_Tw)

Fuentes:
* [A Neolithic Game Board from ʿAin Ghazal, Jordan](https://www.jstor.org/stable/1357113)
* [Brown v. Entertainment Merchants Association](https://www.oyez.org/cases/2010/08-1448)
* [Video Games and the First Amendment: Are Restrictive Regulations Constitutional?](https://repository.law.uic.edu/cgi/viewcontent.cgi?article=1542&context=jitpl)
* [La ética del hacker y el espíritu de la era de la información - Pekka Himanen](http://eprints.rclis.org/12851/1/pekka.pdf)
* [Debugging Game History: A Critical Lexicon, Capítulo 17: Emulation](https://direct.mit.edu/books/book/3495/Debugging-Game-HistoryA-Critical-Lexicon)
* [Art As Experience - John Dewey](https://www.amazon.com.mx/Art-as-Experience-John-Dewey/dp/0399531971)
	* Capítulo 1: The Live Creature
	* Capítulo 2: The Live Creature and Ethereal Things
	* Capítulo 3: Having an Experience
