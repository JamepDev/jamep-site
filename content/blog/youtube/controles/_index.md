---
title: "La ANATOMÍA de los CONTROLES"
date: 2024-06-20
draft: false
---

{{< youtube Y0y_MNv8QxQ >}}

En este video pude relacionar lo que estudio como profesión con mi medio favorito, creo que el enfoque que le di es algo que nunca se ha hablado en la plataforma. Me hubiera gustado terminarlo en menos tiempo, pero mis obligaciones me lo impidieron.

{{< ko-fi >}}

Música:
* [Astronauts And All - Foals](https://youtu.be/1s8iXnVgKcw?si=8WxQnlvgTWzHw7v7)
* [Basic Instinct- Quasimoto](https://www.youtube.com/watch?v=JXeJA2i-Lck)
* [Ready, Able - Grizzly Bear](https://www.youtube.com/watch?v=V8ZVPbSIwI8)
* [Moral Kiosk - R.E.M.](https://www.youtube.com/watch?v=UT4w5e6Ar9I)
* [Qwerty Finger - Everything Everything](https://www.youtube.com/watch?v=j8fSymv-5R8)
* [Electricity - The Avalanches](https://www.youtube.com/watch?v=uPeQ9eP_FgY)
* [Broken Head II - South](https://www.youtube.com/watch?v=PJHe5_aD_Ts)
* [So It Begins - GoGo Penguin](https://www.youtube.com/watch?v=2KUmF5dYJXM)

Fuentes:

[The Semiotics of the Game Controller - Johan Blomberg](https://web.archive.org/web/20231005014658/https://gamestudies.org/1802/articles/blomberg)

[How WASD became the standard PC control scheme](https://www.pcgamer.com/how-wasd-became-the-standard-pc-control-scheme/)

A Fitts’ Law Evaluation of Video Game Controllers: Thumbstick, Touchpad and Gyrosensor - Adrian Ramcharitar & Robert J. Teather.

The Evolution of Game Controllers and Control Schemes and their Effect on their games - Alastair H. Cummings.

Mapping the road to fun: Natural video game controllers, presence, and game enjoyment - Paul Skalski, Ron Tamborini, Ashleigh Shelton, Michael Buncher & Pete Lindmark

Debugging Game History - Controller - Steven E. Jones
