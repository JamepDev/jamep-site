---
title: "Motores De Juego: Una Breve Historia"
date: 2022-11-29
draft: false
---
{{<youtube WWh-0Wj0f6Q>}}

Un vídeo sencillo sobre los motores de juego, 
la idea se me ocurrió mientras leí el origen del término. 
También aproveché este video para experimentar un poco más con mi 
formato que no me gustaba solo poner imágenes en cualquier lugar, así que decidí mostrar imágenes o 
videos como si fuera un tiling window manager, sinceramente si me gustó el resultado.

{{< ko-fi >}}

Música:
* [GoGo Penguin - Time-Lapse City](https://www.youtube.com/watch?v=fkCEASKjdbE)
* [Kokuga OST - 迷妄 (BGM 5)](https://www.youtube.com/watch?v=kElzR5FhXrs)
* [Junior85 - Takeaway or Eat-In (Instrumental)](https://www.youtube.com/watch?v=moxC-yuB7_k)
* [Doom OST - E1M4](https://www.youtube.com/watch?v=43aj3ag3H1c)
* [Junior85 - Left for Deadish](https://www.youtube.com/watch?v=Nei-pz3TLtY)
* [GoGo Penguin - Wave Decay](https://www.youtube.com/watch?v=rzwlh0QagRM)

Fuentes:
* [Debugging Game History: A Critical Lexicon, Capitulo 25: Game Engine](https://direct.mit.edu/books/book/3495/Debugging-Game-HistoryA-Critical-Lexicon)
* [Broderbund Software Catalog](https://archive.org/details/TNM_Broderbund_Software_Information_Packet_and_Overview)
* [The Arcade Machine Manual](https://archive.org/details/ArcadeMachine)
* [3D Construction Kit Manual](https://archive.org/details/3D_Construction_Kit_1991_Domark)
* [Pinball Construction Set Manual](https://archive.org/details/Pinball_Construction_Set_Manual_alt/)
* [Garry Kitchen's GameMaker Manual](https://archive.org/details/A2_Garry_Kitchens_GameMaker_manual/)
* [Thunder Force Construction](https://1-fm--7-com.translate.goog/museum/products/owtr7n0w/?_x_tr_enc=1&_x_tr_sl=ja&_x_tr_tl=en&_x_tr_hl=en)
* [Wargame Construction Kit](https://1-fm--7-com.translate.goog/museum/products/gw2kq490/?_x_tr_enc=1&_x_tr_sl=ja&_x_tr_tl=en&_x_tr_hl=en)
* [Klik & Play Videos](https://archive.org/details/KlikAndPlayVideos/%5BKlik+%26+Play%5D+In-Game+Tutorial-DkUf_mBF72w.mp4)
* [Wargame Construction Set Manual](https://archive.org/details/wargame-construction-set/Wargame%20Construction%20Set%201/)
