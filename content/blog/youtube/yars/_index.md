---
title: "Yar's Revenge: Uno de los mejores titulos de Atari"
date: 2021-07-23
draft: false
---
{{< youtube GyNJHjSzE3I >}}

Este video fue un reto, quería cambiar de estilo a algo más glitch, quise extenderme más a comparación de mis otros videos
 (aunque curiosamente es el guion que más revisiones le he dado XD), 
 y al final de la creación del video puse en práctica la técnica pomodoro para tener una mayor productividad, p
 orque realmente me estaba ganando la procrastinación, y creo que si lo logre, creo que solo tuve un error en un dato, 
 que fue en el inicio con "El juego más vendido de la consola", pero todo lo demás creo que estuvo bien. 
 Por cierto, maldigo a kdenlive porque tuve que renderizar como 6 veces el puto video.

{{< ko-fi >}}


Música:
* [Octahedron OST - Octahedron](https://www.youtube.com/watch?v=33qBCdRsf2w)
* [Octahedron OST - Veetragoul](https://www.youtube.com/watch?v=33qBCdRsf2w)
* [Octahedron OST - Equilibrium](https://www.youtube.com/watch?v=33qBCdRsf2w)

Fuentes:
* [Yars' Revenge](https://archive.org/details/Yars_Revenge_1982)
* [Yars' Revenge: Classic Game Postmortem](https://www.youtube.com/watch?v=aqH4k_OEqhY)
* [Yars' Revenge Extra](https://archive.org/details/yars-revenge-extra-atari-2600_202008/)
* [Yars' Revenge Manual](https://archive.org/details/Yars_Revenge_1981_Atari/)
