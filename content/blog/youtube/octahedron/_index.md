---
title: "El MEJOR PLATAFORMAS que NO CONOCES"
date: 2024-07-12
draft: false
---

{{< youtube 2YUkWGDSGak >}}

Hace mucho que no dedicaba un video a un videojuego, por lo que decidí dedicarle uno a otro de mis favoritos. Investigar sobre este fue bastante enriquecedor. Espero poder convencer a más gente de probar esta gran obra.

{{< ko-fi >}}

Videojuegos:
* Castlevania
* Celeste
* Kirby's Dream Land
* McDonald's Treasure Land Adventure
* Mega Man
* Mega Man 2
* Mega Man 3
* Octahedron: Transfixed Edition
* Pizza Tower
* Super Meat Boy
* Super Mario Bros
* Sonic The Hedgehog
* VVVVVV

Música:
* Octahedron OST - Transfixed
* Octahedron OST - Veetragoul
* Octahedron OST - Valor
* Octahedron OST - Unattached
* Octahedron OST - Sync
* Octahedron OST - Broken
* Octahedron OST - Field Notes

Fuentes:
 * [Octahedron Developer Commentary](https://www.youtube.com/watch?v=ZdaxzIRQ96o)
* [Surviving 2023: A behind-the-scenes look at the state of the gaming industry from an indie studio's perspective - Monomirror](https://web.archive.org/web/20240716214304/https://old.reddit.com/r/IndieDev/comments/18sa0yf/surviving_2023_a_behindthescenes_look_at_the/)