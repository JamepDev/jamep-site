---
title: "LOS VIDEOJUEGOS EN MÉXICO"
date: 2022-05-22
draft: false
---

{{< youtube w0C6R7pSePA >}}

Cada vez mis videos son más largos...

Se me ocurrió la idea cuando estaba leyendo Video Games Around the World, 
y la historia que tenía México estaba interesante, algo digno de divulgarse, además por lo que busque no encontré a alguien más que lo haya hecho.
Como pequeño detalle quise poner pura música hecha en México, a ver si el copyright no es cruel conmigo.

{{< ko-fi >}}


Música:
* [Grupo Kual? - Cumbia en la Playa](https://www.youtube.com/watch?v=RyOaZCCC7sM)
* [Alonso Arreola - C de Cielo](https://www.youtube.com/watch?v=iAH2XUlqRhs)
* [Café Tacvba - El Hombre Impasible](https://www.youtube.com/watch?v=I7hcKaDqf1o)
* [Café Tacvba - [Sin Titulo]](https://www.youtube.com/watch?v=eW7CAxzKoFo)
* [Café Tacvba - El Aparato](https://www.youtube.com/watch?v=C6Xd4AP6QcE)
* [Café Tacvba - El Tlatoani del barrio](https://www.youtube.com/watch?v=FPFh8siipiU)
* [Nortec Collective - No Liazi Jaz](https://youtu.be/nGUuHEichLA)
* [Porter - Murciélago](https://www.youtube.com/watch?v=WquF_9Z9w9Q)
* [Caifanes - Te Estoy Mirando](https://www.youtube.com/watch?v=xoR6WdaI4A0)
* [Caifanes - Mariquita](https://www.youtube.com/watch?v=ndC1XU-CdMY)

Fuentes:
* [Video Games Around the World - Mark J. P. Wolf](https://direct.mit.edu/books/book/3088/Video-Games-Around-the-World)
* [Las olvidadas maquinitas de los noventa - Miguel Ángel Garnica](https://www.eluniversal.com.mx/opinion/mochilazo-en-el-tiempo/las-olvidadas-maquinitas-de-los-noventa/)
* [Consolas Mexicanas - XTOA3](https://www.youtube.com/playlist?list=PLOrxat7kyFnzShaPeXmOUBecC84DamVOP)
* [Industria de Videojuegos en México en 2020 - Alberto Arteaga](https://www.theciu.com/publicaciones-2/2021/3/22/industria-de-videojuegos-en-mxico-en-2020)
* [Take-Two acquires Kerbal Space Program - Matthew Handrahan](https://www.gamesindustry.biz/take-two-acquires-kerbal-space-program)
* [EVOGA, eslabón entre SNK y México - Atomix](https://www.youtube.com/watch?v=wCsbPxhruyo)
* [Radical Studios presenta Eranor - Merca2.0](https://www.merca20.com/radical-studios-presenta-eranor/)
* [Aztec Tech Games](http://www.aztec-tech.com/)
* [SUDA 51 talks about Pato Box - Abraham Ponce](http://bromio.com.mx/blog/suda-51-talks-about-pato-box/)

