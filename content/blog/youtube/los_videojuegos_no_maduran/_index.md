---
title: "Los Videojuegos No Maduran"
date: 2024-12-29
draft: false
---
{{< youtube ebU5SCGtKMQ >}}

Por otros pendientes no pude terminar el video la fecha que me propuse. Además, fue el video con el que me sentí peor haciéndolo, aun a horas de terminarlo siento cansancio y no me apetece pensar más en él. Solo quiero seguir adelante con otros proyectos. Con esto dicho, espero que les haya gustado, y que les agrade el nuevo estilo de edición.

{{< ko-fi >}}

Música:
* [Unreal - Unkle](https://www.youtube.com/watch?v=wk-6vC7-Vyc)
* [Weather Storm - Massive Attack](https://www.youtube.com/watch?v=lTGjFpjOIP8)
* [VW - Late Of The Pier](https://www.youtube.com/watch?v=DdOF_kd7b8o)
* [Dead Cat - Foxxing](https://www.youtube.com/watch?v=3w4nveBdsHs)
* [Far From Grace - Doves](https://www.youtube.com/watch?v=q_yCd5xMKa8)

Fuentes:
* The future of computing beyond Moore’s Law - John Shalf
* Video Games Caught Up in History: Accessibility, Teleological Distortion, and Other Methodological Issues - Carl Therrien
* [Everything Sony Told Us About the Future of PlayStation - Matt Peckham](https://time.com/4804768/playstation-4-ps4-pro-psvr-sales/)
* [Yars' Revenge: Classic Game Postmortem](https://www.youtube.com/watch?v=aqH4k_OEqhY)

