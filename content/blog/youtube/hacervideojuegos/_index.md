---
title: "¿Cómo hacer videojuegos?"
date: 2024-01-19
draft: false
---

{{< youtube LS70iq1d8zE >}}

Este video lo hice un poco para retomar la creación de videojuegos, que ya hace 4 años que no hago uno (como pasa el tiempo). También me parece de provecho compartir lo poco que sé en esta disciplina, espero que le sea de utilidad.

{{< ko-fi >}}

Música:
* [After Hanabi (listen to my beats) - Nujabes](https://www.youtube.com/watch?v=UkhVp85_BnA)
* [Spotlight - Mutemath](https://www.youtube.com/watch?v=a783fVRwtpE)
* [Blue Blood - Foals](https://www.youtube.com/watch?v=rw6oWkCojpw)
* [More than you know - Eddie Vedder](https://www.youtube.com/watch?v=a9rsWxKh2tg)
* [123 4 - Olive Oil](https://www.youtube.com/watch?v=517Iq0W6ePo)
* [Last Call - Kanye West](https://youtu.be/cpbeS15sHZ0)
* [All or nothing - Mutemath](https://youtu.be/yrm4AEzbv2s)

Fuentes:
* [The Incredible Real-Life Story Behind The Friends of Ringo Ishikawa - Jeremy Hosking](https://web.archive.org/web/20190531160041/https://www.kotaku.co.uk/2019/05/30/the-incredible-real-life-story-behind-the-friends-of-ringo-ishikawa)
* [Tweet de Yeo](https://twitter.com/i/status/1719857972560486567)
* [A Reclusive Horror Designer Pivoted From Religious Games - Calum Marsh](https://www.nytimes.com/2023/10/25/arts/five-nights-at-freddys-scott-cawthon.html)
* [Perfil de Zeekerss en roblox](https://www.roblox.com/users/21957385/profile)
* [Perfil de Zeekerss en itch.io](https://zeekerss.itch.io/)
* [Juegos de Zeekerss en steam](https://store.steampowered.com/search/?developer=Zeekerss)
