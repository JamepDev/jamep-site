---
title: "La MENTIRA de los Game Awards"
date: 2023-12-06
draft: false
---
{{<youtube CLaHE5WjhQo>}}

Este video ya lo quería realizar desde el año pasado, y que mejor forma de terminar el año con esto. 
Me gusta el ritmo que estoy tomando al hacer videos, cada vez tengo menos dificultad en hacer algo en menos de un mes, 
el cómo lo escribí me pareció más relajado y la edición fue más sencilla a comparación de otros videos.
En general me gusto mucho el resultado.

{{< ko-fi >}}


Música:
* [Unwound - Lifetime Achievement Award](https://www.youtube.com/watch?v=iDRzrrIlYTE)
* [Back to the Light - Jun Fukuda](https://www.youtube.com/watch?v=yvK20U3dRHA)
* [Jaelyn Nisperos - Cybertropolic Blitz Arrange](https://www.youtube.com/watch?v=tj3PZkPmQVg)
* [High Noon - DJ Shadow](https://www.youtube.com/watch?v=KUNq9XKIRTE)
* [prayer - Takemoto Akira](https://www.youtube.com/watch?v=xt8rixyjp6I)
* [Insignificance - Jim O’Rourke](https://www.youtube.com/watch?v=qNMVaY9ZRpM)
* [Who Cares - Unwound](https://www.youtube.com/watch?v=tJQe5GUAB9w)
 
Fuentes:
* [The Economy of Prestige, Capitulo 1: The Age of Awards - James F. English](https://www.hup.harvard.edu/books/9780674030435)
* [Video game award - Wikipedia](https://en.wikipedia.org/wiki/Video_game_award)
* [List of Game of the Year awards - Wikipedia](https://en.wikipedia.org/wiki/List_of_Game_of_the_Year_awards)
* [Electronic Games - Volume 01 Number 11](https://archive.org/details/Electronic_Games_Volume_01_Number_11_1983-01_Reese_Communications_US/page/n21/mode/2up)
* [Famitsu Issue 0043 (February 19, 1988)](https://www.retromags.com/files/file/4152-famitsu-issue-0043-february-19-1988/)
* [Cybermania '94: The Ultimate Gamer Awards - Chris Nashawaty](https://ew.com/article/1994/11/25/cybermania-94-ultimate-gamer-awards/)
* [Before The Game Awards, There Was Cybermania '94 - Jack Yarwood](https://www.timeextension.com/features/flashback-before-the-game-awards-there-was-cybermania-94)
* [Geoff Keighley’s lifelong obsession to create a video game Oscars - Todd Martens](https://www.latimes.com/entertainment/herocomplex/la-et-hc-game-awards-20171205-story.html)
* [Jurado de los Game Awards](https://thegameawards.com/voting-jury)
* [Tweet de Geoff comparando su premiacion con los oscars](https://twitter.com/geoffkeighley/status/939587654814916608)
* [Tweet de Geoff deseando que su premiacion sea como los oscars](https://twitter.com/geoffkeighley/status/970451212859408384)
