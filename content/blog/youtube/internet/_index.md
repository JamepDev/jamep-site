---
title: "Los videojuegos online ANTES del INTERNET"
date: 2024-07-12
draft: false
---

{{< youtube 6--o59-hVKw >}}

Un video que se me ocurrió al jugar Last Call BBS, y es un tema que he hablado muy superficialmente en otros videos, me pareció interesante dedicarle un video propio. También intenté usar más efectos de sonido, dan un buen añadido.

{{< ko-fi >}}

Música:
* [Galaxy Glitch Groove - Akira Yamaoka](https://youtu.be/wrz0s3GDtZo)
* Black Sky Wiz - Olive Oil
* [the CIA - Glass Beach](https://youtu.be/RxrNNgXP3gQ)
* [Vignette - Twenty One Pilots](https://youtu.be/Nsq_CV9LCCw)
* [PS118 - John Fio](https://youtu.be/u9i0bEe0640)

Fuentes:

* Before the Crash: Early Video Game History | Early Online Gaming - Staci Trucker
* [1991: Trade Wars 2002 - Aaron A. Reed](https://if50.textories.com/files/50YearsOfTextGames-TradeWars.pdf)
* [Computers and automation Vol. 9](https://bitsavers.org/magazines/Computers_And_Automation/196202.pdf)
* Designing Virtual Worlds - Richard A. Bartle