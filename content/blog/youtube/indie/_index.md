---
title: "El vacío del indie"
date: 2021-12-16
draft: false
---
{{< youtube 1VRCZ2yZQ6U >}}

Este video surgió al ver la conferencia de Bennett Foddy sobre el indie, el tipo me transmitió un gran entusiasmo y 
me puse a indagar más sobre esto y como vi que la mayoría de contenido en español apenas cuestiona esto, pues me lancé al pozo.
El guion inició desde septiembre, pero recuerdo hacerle varias revisiones y cambios, después tuve que retrasos en la edición del video por mi estupidez
(rendericé como 4 veces el video), algo de mi perfeccionismo y los estudios. Pero en general creo que quedo piola el video.

{{< ko-fi >}}


Música:
* [Ramble Planet OST - Title (El link te descarga todo el ost, porque no encontre un video en youtube)](http://files.andihagen.com/miscmedia/rambleplanetost.zip)
* [Ramble Planet OST - Big Wisp (Lo mismo que arriba)](http://files.andihagen.com/miscmedia/rambleplanetost.zip)
* [Earthsuit - Hutch Buggin 2](https://youtu.be/XH2MmH-jkng?t=2231)
* [Earthsuit - Hutch Buggin 3](https://youtu.be/XH2MmH-jkng?t=2706)
* [Flower, Sun and Rain OST - KATHERINE #1](https://www.youtube.com/watch?v=U8AihjGlHxw)
* [The Silver Case OST - Morishima Tokio](https://www.youtube.com/watch?v=BF1Sehb_LRU)
* [Octahedron OST - Ransom](https://youtu.be/bnqhhStV_RM?t=2720)
* [Ghost Trick OST - Jowd ~A Captive of Fate](https://youtu.be/ytgMJYZcNK4)
* [MUTEMATH - Japan](https://www.youtube.com/watch?v=9MN1MFr4QRk)

Fuentes:
* [Indiecade East 2014: State of the Union - Bennett Foddy](https://www.youtube.com/watch?v=9jocCmSS8ds)
* [Handmade Pixels: Quest for making more authentic games - Jesper Juul](https://www.youtube.com/watch?v=9jocCmSS8ds)
* [BOOK REVIEW: HANDMADE PIXELS: INDEPENDENT VIDEO GAMES AND THE QUEST FOR AUTHENTICITY -  Emilie Reed](https://web.archive.org/web/20211011175635/http://gamescriticism.org/reviews/reed-4-1)
* [What Does It Really Mean to Be an Indie Game? - Cameron Kunzelman](https://www.vice.com/en/contributor/cameron-kunzelman)
* [What is Indie? - Fred Dutton](https://www.eurogamer.net/what-is-indie)
* [About Game Tunnel](https://web.archive.org/web/20090228092120/http://www.gametunnel.com/about-game-tunnel-article.php)
* [Glorius Trainwrecks](https://www.glorioustrainwrecks.com/)
* [El "Indie" NO ES UN GENERO MUSICAL](https://www.youtube.com/watch?v=Fz1-hh9nwmo)
* [How the Sony PlayStation Net Yaroze DevKit brought Indie Game Development to Consoles - MVG](https://www.youtube.com/watch?v=rtE5hmlrcBo)
* [Net Yaroze Spanish Tutorial Revista Super Juegos](https://archive.org/details/tutorial-net-yaroze-revista-super-juegos/mode/2up)
